from ... import db
from datetime import datetime
class so_Report(db.Model):
    __tablename__ = 'so_report_test'

    id = db.Column(db.INTEGER, primary_key=True, autoincrement=True)
    name = db.Column(db.VARCHAR(25), nullable=False,default='Unnamed')
    report_dt = db.Column(db.DATETIME,nullable=False,default=datetime.utcnow)
    login = db.Column(db.VARCHAR(25),nullable=False)

#
#
#    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     name = db.Column(db.String, nullable=False)
#     agg_fact_hc = db.Column(db.Float, nullable=False,default=0)
#     agg_source_hc = db.Column(db.Float, nullable=False,default=0)
#     agg_vacancy_hc = db.Column(db.Float, nullable=False,default=0)
#     count_rows = db.Column(db.Integer,nullable=False,default=0)
#     status_id = db.Column(db.Integer,nullable=False)
#     last_changes = db.Column(db.DateTime,nullable=False,default=datetime.utcnow)
#     last_login = db.Column(db.String(25),nullable=False,default='Unnamed')

#   id = db.Column(db.Integer, primary_key=True, autoincrement=True)
#     email = db.Column(db.String(255), unique=True, nullable=False)
#     registered_on = db.Column(db.DateTime, nullable=False)
#     admin = db.Column(db.Boolean, nullable=False, default=False)
#     public_id = db.Column(db.String(100), unique=True)
#     username = db.Column(db.String(50), unique=True)
#     password_hash = db.Column(db.String(100))
#
#
# class Address(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     email = db.Column(db.String(120), nullable=False)
#     person_id = db.Column(db.Integer, db.ForeignKey('person.id'),
#         nullable=False)

# Table so_report{
#   id int
#   name varchar(50)
#   status int
#   report_dt datetime
#   login varchar(25)
# }