from flask_restplus import Resource
from flask import jsonify, request
from ..services.report_service import create_report, get_all_reports, \
    delete_report
from ...parsers.file_parser import ExcelParser, InputParser
from . import apiTest

local_route = '/reports'

excel = ExcelParser()
excel.set_parser('name', type=str, required=False, help='Report name')

report_parser = InputParser()
report_parser.set_parser('name', type=str, required=False, help='Report name')


@apiTest.route(local_route)
class Reports(Resource):
    @apiTest.doc('list_reports')
    def get(self):
        """Get all reports"""
        return {'reports': get_all_reports()}, 200

    @apiTest.response(201, 'Report successfully created.')
    @apiTest.doc('create_report')
    @apiTest.expect(report_parser.get_parser())
    def post(self):
        """Create a new report """
        report_name = report_parser.get_args()['name']
        response = create_report(report_name)
        return response, 201


@apiTest.route(f'{local_route}/<int:id>')
@apiTest.param('id', 'The report identifier')
class Report(Resource):
    @apiTest.doc('delete_report')
    @apiTest.response(204, 'Report deleted')
    def delete(self, id):
        '''Delete a report by id'''
        delete_report(id)
        return '', 204

    @apiTest.doc('update_report')
    @apiTest.response(204, 'Report updated')
    def put(self, id):
        '''Update a report by id'''

        return 10, 204
