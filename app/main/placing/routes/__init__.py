from flask_restplus import Namespace

apiTest = Namespace('source_mapper', description='test',
                path='/source_mapper')

from . import reports

blocks_locations_api = Namespace('blocks_locations', description='placing block on locations',
                path='/blocks_locations')
from .blocks_locations import scenarios, placing_variants

departments_buildings_api = Namespace('departments_buildings', description='placing departments on buildings',
                path='/departments_buildings')