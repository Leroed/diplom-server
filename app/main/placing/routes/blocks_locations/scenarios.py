from flask_restplus import Resource
from app.main.parsers.file_parser import InputParser
from app.main.placing.routes import blocks_locations_api as api
from app.main.placing.services.blocks_locations import get_blocks, get_locations
from app.main.placing.services.blocks_locations.scenarios import get_scenarios

local_route = '/scenarios'
scenarios_parser = InputParser()
scenarios_parser.set_parser('name', type=str, required=False, help='Report name')


@api.route(local_route)
class Reports(Resource):
    @api.doc('scenario variants')
    def get(self):
        return {'blocks': get_blocks(), 'scenarios': get_scenarios(), 'locations': get_locations() }, 200


