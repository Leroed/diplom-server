from flask_restplus import Resource
from app.main.parsers.file_parser import InputParser
from app.main.placing.routes import blocks_locations_api as api
from app.main.placing.services.blocks_locations import get_locations, get_blocks
from app.main.placing.services.blocks_locations.placing_variants import get_placing_variants_locations

local_route = '/placing-variants'
placing_variants_parser = InputParser()
placing_variants_parser.set_parser('id', type=str, required=True, help='Scenario id')


@api.route(local_route)
class Reports(Resource):
    @api.doc('create_report')
    @api.expect(placing_variants_parser.get_parser())
    def post(self):
        scenario_id = placing_variants_parser.get_args()['id']
        response = get_placing_variants_locations()
        return {'blocks': get_blocks(), 'rows': get_placing_variants_locations(), 'locations': get_locations() }, 200



