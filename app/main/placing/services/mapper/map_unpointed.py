from .map_algorythm import SourceMapper, NewSource


class UnpointedSourceMapper(SourceMapper):
    def __init__(self, new_report: NewSource):
        super(UnpointedSourceMapper, self).__init__(new_report)
        self._actual = 0


class Interns(UnpointedSourceMapper):
    def __init__(self, new_report: NewSource):
        super(Interns, self).__init__(new_report)
        self.title = 'Стажеры'
        search_cols = self.get_columns_by_regex(self.unmapped_df.columns,
                                                'so_position_name')
        intern_filter = self.get_filter_by_regex_rows(self.unmapped_df,
                                                      search_cols, 'Стажер')
        self.unmapped_df = self.unmapped_df.loc[intern_filter, :]
        self.set_block()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()

    def set_block(self):
        mask = self.unmapped_df['so_fb_oss'].isnull()
        self.unmapped_df['so_block_report'] = self.unmapped_df[
            'so_fb_oss'].where(
            mask == False, self.unmapped_df['so_fb_type'])

    # def set_level(self):
    #     lvls = ['so_type_lvl_1_name','so_lvl_2_name','so_lvl_1_name']
    #     for l in lvls:
    #         self.unmapped_df['so_lvl_report'] = self.unmapped_df.apply(
    #             lambda row: func(row))

    # def get_level(self):


class Deleted(UnpointedSourceMapper):
    def __init__(self, new_report: NewSource):
        super(Deleted, self).__init__(new_report)
        self.title = 'Упраздненные подразделения'
        search_cols = self.get_columns_by_regex(self.unmapped_df.columns,
                                                'so_type_')
        deleted_filter = self.get_filter_by_regex_rows(self.unmapped_df,
                                                       search_cols, 'Упразд')
        hc_filter = self.slice_deleted_rows()

        self.unmapped_df = self.unmapped_df.loc[(deleted_filter) |
                                                (hc_filter), :]

        self.add_info_tag(self.unmapped_df)
        self.add_actual()
        self.update_df()

    def slice_deleted_rows(self):
        filter = ((self.unmapped_df['so_source_hc'] == 0) & (
                self.unmapped_df['so_fact_hc'] > 0))
        return filter
