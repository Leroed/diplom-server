from .map_algorythm import SourceMapper, NewSource, PreviousSource
import Levenshtein
from tqdm import tqdm
import pandas as pd


class ActualSourceMapper(SourceMapper):
    def __init__(self, new_report: NewSource, previous_report: PreviousSource):
        super(ActualSourceMapper, self).__init__(new_report, previous_report)
        self._actual = 1


class OrgPos(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(OrgPos, self).__init__(new_report, previous)
        self.title = 'map by org_id + pos_id'
        self.merged_cols = ['so_org_id', 'so_position_id']
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()


class TypeLvls(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(TypeLvls, self).__init__(new_report, previous)
        self.title = 'map by type lvls + pos_id'
        self.merged_cols = ['so_position_id']
        self.merged_cols.extend(self.get_columns_by_regex(
            self.unmapped_df.columns, 'so_type_'))
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()


class CaTypeLvls(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(CaTypeLvls, self).__init__(new_report, previous)
        self.unmapped_df = self.unmapped_df[self.get_only_ca()]
        self.title = 'map by type lvls (only ca)'
        self.merged_cols = self.get_columns_by_regex(
            self.unmapped_df.columns, 'so_type_')
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()

    def get_only_ca(self):
        filter = self.unmapped_df[
                     'so_type_lvl_1_name'] == 'ЦЕНТРАЛЬНЫЙ АППАРАТ'
        return filter


class OrgLvls(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(OrgLvls, self).__init__(new_report, previous)
        self.title = 'map by org lvls + pos_id'
        self.merged_cols = ['so_position_id']
        self.merged_cols.extend(self.get_columns_by_regex(
            self.unmapped_df.columns, 'so_lvl_'))
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()


class OrgLvls(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(OrgLvls, self).__init__(new_report, previous)
        self.title = 'map by org lvls + pos_id'
        self.merged_cols = ['so_position_id']
        self.merged_cols.extend(self.get_columns_by_regex(
            self.unmapped_df.columns, 'so_lvl_'))
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()


class SingleRoles(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(SingleRoles, self).__init__(new_report, previous)
        self._single_orgs = self.previous.get_single_role_org_id()
        self.edit_prev_df = self.edit_prev_df[self.edit_prev_df[
            'so_org_id'].isin(self._single_orgs)]
        self.title = 'map by org_id (single roles)'
        self.merged_cols = ['so_org_id']
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()


class LevenshteinMap(ActualSourceMapper):
    def __init__(self, new_report: NewSource, previous: PreviousSource):
        super(LevenshteinMap, self).__init__(new_report, previous)
        self.title = 'map by levenshtein (org lvls)'
        self.merged_cols = self.get_columns_by_regex(
            self.unmapped_df.columns, 'so_lvl_')
        self.unique_new = self.concat_columns(self.unmapped_df, 'key')
        self.unique_old = self.concat_columns(self.edit_prev_df,
                                              'previous_key')
        self.get_distance()
        self.merged_cols = ['key']
        self.merge_by_previous_period()
        self.add_info_tag(self.unmapped_df)
        self.add_actual()

        self.update_df()

    def concat_columns(self, df: pd.DataFrame, key_name):
        df.drop_duplicates(subset=self.merged_cols, inplace=True)
        df[key_name] = df.index
        df['CONCATED'] = df.apply(
            lambda x: self.concat_source(x, self.merged_cols), axis=1)
        return df[[key_name, 'CONCATED']].sort_values(by=['CONCATED'])

    def concat_source(self, row, columns):
        if str(row['so_type_lvl_1_name']) == 'ВСП':
            return row[columns + ['so_position_name']].str.cat(sep='',
                                                               na_rep='')
        else:
            return row[columns].str.cat(sep='', na_rep='')

    def get_distance(self):
        self.add_new_columns(self.edit_prev_df, 'key')
        for new_row in tqdm(self.unique_new.itertuples(),
                            total=self.unique_new.shape[0]):
            for i, x in self.unique_old.iterrows():
                if self.lev(x, new_row, 0.85):
                    break

    def lev(self, x: str, compare_str: str, diff: float):
        search_col = 'CONCATED'
        compare_concat = getattr(compare_str, search_col)
        dist = Levenshtein.distance(x[search_col], compare_concat)
        max_len = max(len(x[search_col]), len(compare_concat))
        val = 1 - dist / max_len
        if val >= diff:
            key = int(getattr(compare_str, 'key'))
            mask = self.edit_prev_df['previous_key'] == x['previous_key']
            self.edit_prev_df.loc[mask, 'key'] = key
            return True
