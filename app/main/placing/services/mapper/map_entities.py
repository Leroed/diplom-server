import pandas as pd

from . import COLUMNS_DZO, COLUMNS_PAO, \
    COLUMNS_TYPES
from .map_algorythm import PreviousSource, NewSource
from .map_actual import OrgPos, TypeLvls, CaTypeLvls, OrgLvls, SingleRoles, \
    LevenshteinMap
from .map_unpointed import Interns, Deleted
from .map_manual import ManualSourceMapper
from .map_counter import CountFactFte
from typing import List


class MapEntity:
    def __init__(self, df: pd.DataFrame,
                 previous_df: pd.DataFrame = pd.DataFrame(),
                 base_columns: List = [],
                 filters: pd.DataFrame = pd.DataFrame(),
                 celery_task=False, if_exists='append'):
        self.df = df
        self.celery_task = celery_task
        self.hash_key_columns = []
        self.hash_filter_column = None
        self.rules = dict()
        self.counters = dict()
        self.filters = filters
        self.if_exists = if_exists
        self.info = dict()
        self.new_report = NewSource(self.df, base_columns)
        self.previous_report = PreviousSource(previous_df)
        self.previous_report.set_column_types(COLUMNS_TYPES)

    def complete_counters(self):
        for counter in self.counters:
            step = counter['instance'](self.new_report)
            if self.celery_task:
                self.celery_task.update_state(state='COUNTING',
                                              meta={})

    def complete_rules(self):
        for rule in self.rules:
            if rule.get('previous', 0):
                step = rule['instance'](self.new_report, self.previous_report)
            elif rule.get('filters', 0):
                step = rule['instance'](self.new_report, self.filters)
            else:
                step = rule['instance'](self.new_report)
            if self.celery_task:
                self.celery_task.update_state(state='MAPPING',
                                              meta=step.get_status)
        self.hash_filter_column = step.info_tag

    def set_hash(self):
        self.info['unmapped_rows'] = 0
        if self.hash_filter_column:
            self.new_report.df['hash_key'] = self.new_report.df \
                .apply(lambda row: self.get_hash(row), axis=1)

    def get_hash(self, row):
        if not pd.isna(row[self.hash_filter_column]):
            concat_key = ''
            for col in self.hash_key_columns:
                concat_key += str(row[col])
            return hash(concat_key)
        else:
            self.info['unmapped_rows'] += 1


class PaoMapper(MapEntity):
    def __init__(self, df, previous_df: pd.DataFrame = pd.DataFrame(),
                 celery_task=False,
                 rules:List=None, filters: pd.DataFrame = pd.DataFrame(),
                 if_exists='append'):
        super(PaoMapper, self).__init__(df, previous_df=previous_df,
                                        base_columns=COLUMNS_PAO,
                                        celery_task=celery_task,
                                        filters=filters,
                                        if_exists=if_exists)
        self.hash_key_columns = ['so_org_id', 'so_position_id']
        if self.if_exists == 'append':
            self.rules = [
                {'instance': Interns, 'previous': False},
                {'instance': Deleted, 'previous': False},
                {'instance': OrgPos, 'previous': True},
                {'instance': TypeLvls, 'previous': True},
                {'instance': CaTypeLvls, 'previous': True},
                {'instance': OrgLvls, 'previous': True},
                {'instance': SingleRoles, 'previous': True},
                # {'instance': LevenshteinMap, 'previous': True},
                {'instance': ManualSourceMapper, 'filters': True}
            ]
            self.counters = [
                {'instance': CountFactFte},
            ]
        else:
            self.rules = rules
        self.complete_rules()
        self.complete_counters()
        self.set_hash()
        self.info.update(self.new_report.info)


class DzoMapper(MapEntity):
    def __init__(self, df, previous_df: pd.DataFrame = pd.DataFrame(),
                 celery_task=False,
                 rules=None, if_exists='append'):
        super(DzoMapper, self).__init__(df, previous_df=previous_df,
                                        base_columns=COLUMNS_DZO,
                                        celery_task=celery_task,
                                        if_exists=if_exists)
        self.hash_key_columns = ['so_dzo_name']
        if self.if_exists == 'append':
            self.counters = [
                {'instance': CountFactFte},
            ]

        self.complete_rules()
        self.set_hash()
        self.info = self.new_report.info
