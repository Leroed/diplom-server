import pandas as pd
import numpy as np
from typing import List, Dict
import re
from . import RESULT_COLUMNS
from itertools import product
from app.main.util.dataframes.dataframe import DF
from .map_setter import SourceCleanDF


class PreviousSource(DF):
    def __init__(self, df):
        super(PreviousSource, self).__init__(df)
        if len(self.df.index) > 0:
            self.get_notna_rows()

    def get_notna_rows(self):
        nan_rows = ['Стажеры', 'Упраздненные подразделения']
        self.df = self.df.loc[~self.df['so_direction_report'].isin(
            nan_rows), :]

    def get_single_role_org_id(self):
        single_df = self.df.drop_duplicates(subset=['so_org_id',
                                                    'so_code_role'])
        single_df = single_df[single_df['so_type_lvl_1_name'] ==
                              'ЦЕНТРАЛЬНЫЙ АППАРАТ']
        group_df = single_df[['so_org_id', 'so_code_role']].groupby(
            by=['so_org_id']).count().reset_index()
        single_roles = group_df[group_df['so_code_role'] == 1]
        return single_roles['so_org_id'].unique().tolist()


class NewSource(SourceCleanDF):
    def __init__(self, df, base_columns=None):
        super(NewSource, self).__init__(df, base_columns)
        self.df['id'] = self.df.index + 1
        self._info = dict()

    def set_info(self):
        self._info['count_rows'] = self.df.shape[0]
        self._info['date'] = self.report_date
        self._info['agg_fact_hc'] = self.df['so_fact_hc'].sum().round(2)
        self._info['agg_source_hc'] = self.df['so_source_hc'].sum().round(2)
        self._info['agg_vacancy_hc'] = self.df['so_vacancy_hc'].sum().round(2)

    @property
    def info(self):
        self.set_info()
        return self._info


class SourceMapper(DF):
    def __init__(self, new_report: NewSource, previous: PreviousSource = None):
        super(SourceMapper, self).__init__(new_report.df)
        self.title = None
        self.info_tag = 'INFO'
        self.actual_tag = 'so_actual'
        self.necessary_columns = RESULT_COLUMNS
        self.previous = previous
        if self.previous:
            self.edit_prev_df = self.previous.df.copy(deep=True)
        self.merged_cols = None
        self.unmapped_df = self.get_unmapped_df()
        self.len_before = self.unmapped_df.shape[0]
        self._status = dict()
        self._actual = None

    def get_unmapped_df(self):
        """

        :return:
        """
        if self.info_tag in self.df.columns:
            filter = self.df[self.info_tag].isnull()
            return self.df.loc[filter, :]
        else:
            self.add_new_columns(self.df, self.info_tag, self.actual_tag)
            return self.df

    def merge_by_previous_period(self):
        self.drop_duplicated_by_merge_cols()
        self.unmapped_df['key'] = self.unmapped_df.index
        base_columns = [c for c in self.unmapped_df.columns if
                        c not in self.necessary_columns]
        self.unmapped_df = self.unmapped_df[base_columns]
        need_cols = self.merged_cols + self.necessary_columns
        self.unmapped_df = self.unmapped_df.merge(self.edit_prev_df[need_cols],
                                                  on=self.merged_cols,
                                                  how='left')
        self.get_duplicated_rows()
        self.unmapped_df.set_index('key', inplace=True)
        self.get_mapped_rows()

    def get_duplicated_rows(self):
        duplicated_rows = self.unmapped_df.duplicated(subset='key')
        duplicated_df = self.unmapped_df[duplicated_rows]
        if len(duplicated_df.index) > 0:
            print(self.title)
            duplicated_df.to_excel(f'FAKE_{self.title}.xlsx')
        self.unmapped_df.drop_duplicates(subset='key', inplace=True)

    def get_mapped_rows(self):
        check_columns = ['so_code_role', 'so_name_role']
        arrays_by_cols = [self.unmapped_df[col].notna()
                          for col in check_columns]
        filter = np.column_stack(arrays_by_cols)
        result = filter.any(axis=1)
        self.unmapped_df = self.unmapped_df.loc[result, :]

    def drop_duplicated_by_merge_cols(self):
        if all(elem in self.edit_prev_df.columns for elem in self.merged_cols):
            self.edit_prev_df.drop_duplicates(subset=self.merged_cols,
                                              inplace=True)

    @staticmethod
    def get_columns_by_regex(columns: List[str],
                             regex_key: str) -> List[str]:
        """

        :param columns:
        :param regex_key:
        :return:
        """
        r = re.compile(f'{regex_key}*')
        return list(filter(r.match, columns))

    @staticmethod
    def get_filter_by_regex_rows(df: pd.DataFrame, iter_columns: List[str],
                                 regex_key: str) -> pd.DataFrame:
        """

        :param df:
        :param iter_columns:
        :param regex_key:
        :return:
        """
        arrays_by_cols = [df[col].str.contains(f'^{regex_key}', na=False)
                          for col in iter_columns]
        filter = np.column_stack(arrays_by_cols)
        result = filter.any(axis=1)
        return result

    def add_info_tag(self,df:pd.DataFrame):
        if len(df.index) > 0:
            self.add_new_columns(df, self.info_tag)
            df.loc[:, self.info_tag] = self.title

    def add_status(self):
        len_all = self.df.shape[0]
        len_done_now = self.unmapped_df.shape[0]
        len_done_before = len_all - self.len_before
        self._status['all'] = len_all
        self._status['done'] = len_done_now + len_done_before
        self._status['title'] = self.title

    def add_actual(self):
        col = self.actual_tag
        self.add_new_columns(self.unmapped_df, col)
        self.unmapped_df[col] = self._actual

    @property
    def get_status(self):
        self.add_status()
        return self._status

    def update_df(self):
        unknown_c = list(set(self.unmapped_df.columns) - set(self.df.columns))
        need_cols = [c for c in unknown_c if c in self.necessary_columns]
        self.add_new_columns(self.df, *need_cols)
        self.df.update(self.unmapped_df)

    def set_static_block(self):
        pass

    def set_static_lvl(self):
        pass

    def set_static_direction(self):
        column = 'so_direction_report'
        self.unmapped_df[column] = self.title


