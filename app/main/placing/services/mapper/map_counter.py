from app.main.util.dataframes.dataframe import DF


class CountFactFte(DF):
    def __init__(self, report:DF):
        super(CountFactFte, self).__init__(report.df)
        self.add_new_columns(self.df, 'INFO')
        self.df['so_fact_fte'] = self.df.apply(lambda x: self.get_fact_fte(
            x), axis=1)

    def get_fact_fte(self, row):
        if row['INFO'] == 'Упраздненные подразделения':
            return row['so_fact_hc']
        else:
            return row['so_source_hc'] - row['so_vacancy_hc']
