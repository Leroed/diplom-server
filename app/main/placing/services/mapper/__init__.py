COLUMNS_TYPES = {
    'so_date': 'str',
    'so_fb_oss': 'str',
    'so_fb_type': 'str',
    'so_tb_name': 'str',
    'so_gosb_name': 'str',
    'so_lvl_1_name': 'str',
    'so_lvl_2_name': 'str',
    'so_lvl_3_name': 'str',
    'so_lvl_4_name': 'str',
    'so_lvl_5_name': 'str',
    'so_lvl_6_name': 'str',
    'so_lvl_7_name': 'str',
    'so_lvl_8_name': 'str',
    'so_lvl_9_name': 'str',
    'so_lvl_10_name': 'str',
    'so_lvl_11_name': 'str',
    'so_lvl_12_name': 'str',
    'so_lvl_13_name': 'str',
    'so_lvl_14_name': 'str',
    'so_lvl_15_name': 'str',
    'so_type_lvl_1_name': 'str',
    'so_type_lvl_2_name': 'str',
    'so_type_lvl_3_name': 'str',
    'so_type_lvl_4_name': 'str',
    'so_type_lvl_5_name': 'str',
    'so_type_lvl_6_name': 'str',
    'so_type_lvl_7_name': 'str',
    'so_region_name': 'str',
    'so_code_gosb': 'float',
    'so_code_gosb_new': 'str',
    'so_position_name': 'str',
    'so_org_id': 'int',
    'so_position_id': 'float',
    'so_fact_hc': 'float',
    'so_source_hc': 'float',
    'so_vacancy_hc': 'float'
}
COLUMNS_ROLES = {
    'Code': ['Код роли'],
    'Name': ['Имя роли'],
    'Code_Block': ['Код блока'],
}

RESULT_COLUMNS = ['so_report_lvl', 'so_direction_report',
                  'so_block_report', 'so_name_role',
                  'so_code_role']
COLUMNS_PAO = {
    'so_date': ['Отчетная дата'],
    'so_fb_oss': ['ФБ по ОШС'],
    'so_fb_type': ['ФБ по типовой'],
    'so_tb_name': ['ТБ, наименование'],
    'so_gosb_name': ['ГОСБ, наименование'],
    'so_lvl_1_name': ['Ур. подразделения 1, наименование'],
    'so_lvl_2_name': ['Ур. подразделения 2, наименование'],
    'so_lvl_3_name': ['Ур. подразделения 3, наименование'],
    'so_lvl_4_name': ['Ур. подразделения 4, наименование'],
    'so_lvl_5_name': ['Ур. подразделения 5, наименование'],
    'so_lvl_6_name': ['Ур. подразделения 6, наименование'],
    'so_lvl_7_name': ['Ур. подразделения 7, наименование'],
    'so_lvl_8_name': ['Ур. подразделения 8, наименование'],
    'so_lvl_9_name': ['Ур. подразделения 9, наименование'],
    'so_lvl_10_name': ['Ур. подразделения 10, наименование'],
    'so_lvl_11_name': ['Ур. подразделения 11, наименование'],
    'so_lvl_12_name': ['Ур. подразделения 12, наименование'],
    'so_lvl_13_name': ['Ур. подразделения 13, наименование'],
    'so_lvl_14_name': ['Ур. подразделения 14, наименование'],
    'so_lvl_15_name': ['Ур. подразделения 15, наименование'],
    'so_type_lvl_1_name': ['Типовой уровень 1, наименование'],
    'so_type_lvl_2_name': ['Типовой уровень 2, наименование'],
    'so_type_lvl_3_name': ['Типовой уровень 3, наименование'],
    'so_type_lvl_4_name': ['Типовой уровень 4, наименование'],
    'so_type_lvl_5_name': ['Типовой уровень 5, наименование'],
    'so_type_lvl_6_name': ['Типовой уровень 6, наименование'],
    'so_type_lvl_7_name': ['Типовой уровень 7, наименование'],
    'so_region_name': ['Область'],
    'so_code_gosb': ['Код ГОСБ'],
    'so_code_gosb_new': ['Код ГОСБ_испр'],
    'so_position_name': ['Эталонная должность, наименование'],
    'so_org_id': ['Организационная единица'],
    'so_position_id': ['Эталонная штатная должность'],
    'so_fact_hc': ['Фактическая численность'],
    'so_source_hc': ['Штатная численность'],
    'so_vacancy_hc': ['Вакансии'],
}

COLUMNS_DZO = {
    'so_date': ['Отчетная дата'],
    'so_dzo_name': ['Наименование ДЗО'],
    'so_func_block': ['Функциональный блок'],
    'so_fact_hc': ['Фактическая численность'],
    'so_source_hc': ['Штатная численность'],
    'so_vacancy_hc': ['Вакансии'],
}


class EntityStatus:
    STATUS = None

    @property
    def status(self):
        return self.STATUS


class CalculatedStatus(EntityStatus):
    STATUS = 'CALCULATED'


class CreatedStatus(EntityStatus):
    STATUS = 'CREATED'


class DoneStatus(EntityStatus):
    STATUS = 'DONE'
