import pandas as pd
import os

from app.main import celery
from .map_entities import PaoMapper, DzoMapper, MapEntity
from app.main.util.files import parse_xlsx_to_csv, get_file_extension, \
    get_entity_file
from flask import current_app
from . import CalculatedStatus, CreatedStatus, DoneStatus


# @celery.task(bind=True)
# def placing(self, filename, entity_key):
#     ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
#     OLD_FILE_PATH = os.path.join(ROOT_DIR, '3.csv')
#     self.update_state(state='READING FILE', meta={})
#     if get_file_extension(filename) == '.xlsx':
#         csv_path = parse_xlsx_to_csv(filename)
#         os.remove(filename)
#     df = pd.read_csv(csv_path)
#     old = pd.read_csv(OLD_FILE_PATH)
#     os.remove(csv_path)
#
#     entities = {1: PaoMapper, 2: DzoMapper}
#     mapper = entities[entity_key](df, old, self)
#
#     self.update_state(state='LOAD TO DB', meta={})
#     mapper.new_df.to_csv('2.csv', index=False)
#
#     return {'status': 'done'}


def source_mapper(filename, entity_key, report_id, previous_id,
                  if_exists='append'):
    if get_file_extension(filename) == '.xlsx':
        df = pd.read_excel(filename)
        os.remove(filename)

    previous_report = get_entity_file(previous_id)
    if previous_report:
        old = pd.read_csv(previous_report, thousands='.')

    entities = {1: PaoMapper, 2: DzoMapper}
    mapper = entities[entity_key](df=df, previous_df=old, if_exists=if_exists)
    info_report = complete_mapper(mapper=mapper, entity_key=entity_key,
                                  report_id=report_id,
                                  if_exists=if_exists)
    return info_report


def complete_mapper(mapper: MapEntity, entity_key=None, report_id=None,
                    id=None, if_exists='append'):
    info_report = mapper.info
    info_report['login'] = 'admin'
    info_report['status'] = CalculatedStatus().status if \
        info_report['unmapped_rows'] > 0 else DoneStatus().status

    con = current_app.config['ENGINE'].raw_connection()
    cursor = con.cursor()
    if if_exists == 'append':
        info_report['report_id'] = report_id
        info_report['entity_id'] = entity_key
        insert_q = f"""
            INSERT INTO so_report_entity(date,count_rows,unmapped_rows,agg_fact_hc,
            agg_source_hc, agg_vacancy_hc,report_id,entity_id,login,status)
            OUTPUT Inserted.ID
            VALUES('{info_report['date']}',{info_report['count_rows']},
                    {info_report['unmapped_rows']},
                    {info_report['agg_fact_hc']},{info_report['agg_source_hc']},
                    {info_report['agg_vacancy_hc']},{info_report['report_id']},
                    {info_report['entity_id']},'{info_report['login']}',
                    '{info_report['status']}')"""
        cursor.execute(insert_q)
        inserted_id = cursor.fetchone()[0]
        entity_path = os.path.join(current_app.config['ENTITIES_FOLDER'],
                                   f'{inserted_id}.csv')
    elif if_exists == 'replace':
        info_report['id'] = id
        update_q = f"""
            UPDATE so_report_entity 
            SET unmapped_rows={info_report['unmapped_rows']},
                status='{info_report['status']}',
                login='{info_report['login']}'
            WHERE id = {info_report['id']}"""
        cursor.execute(update_q)
        entity_path = os.path.join(current_app.config['ENTITIES_FOLDER'],
                                   f'{id}.csv')
    cursor.commit()
    cursor.close()

    mapper.new_report.df.to_csv(entity_path, index=False)
    return info_report
