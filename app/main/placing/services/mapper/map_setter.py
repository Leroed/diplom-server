from app.main.util.dataframes.clean_dataframe import CleanDF
from typing import Dict
import pandas as pd
from app.main.placing.services.mapper import COLUMNS_TYPES
import datetime as dt
from flask import current_app


class SourceCleanDF(CleanDF):
    def __init__(self, df, columns=None):
        super(SourceCleanDF, self).__init__(df, columns)
        if columns:
            self.df.columns = self.set_column_names(columns)
        self._report_date = self.set_report_date()
        self.set_column_types(COLUMNS_TYPES)
        # template_types = self.get_column_types()
        # self.set_column_types(template_types)

    @staticmethod
    def get_column_types(catalog: str = 'Headcount',
                         table: str = 'test') -> Dict:
        """
        Get types of template placing columns
        :param catalog: Database's name
        :param table: Table's name
        :return: Dict column: type column
        """
        query = f"""SELECT top 1 * FROM [{catalog}].[dbo].[{table}]"""
        column_types = pd.read_sql(query, current_app.config['ENGINE'])
        return dict(zip(column_types.columns, column_types.dtypes))

    def set_report_date(self):
        col_date = 'so_date'
        if self.df[col_date].dtype == float:
            self.df[col_date] = pd.TimedeltaIndex(self.df[col_date],
                                                  unit='d') + dt.datetime(1899,
                                                                          12,
                                                                          30)
        self.df[col_date] = self.df[col_date].astype(str)
        report_date = self.df[col_date].unique().tolist()
        if len(report_date) > 1:
            raise ValueError
        report_date = report_date[0]
        return report_date

    @property
    def report_date(self):
        return self._report_date
