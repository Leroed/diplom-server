import pandas as pd
import numpy as np
from typing import List, Dict
import re
from .map_algorythm import SourceMapper, NewSource
from flask import current_app


class ManualSourceMapper(SourceMapper):
    def __init__(self, new_report: NewSource, filters: pd.DataFrame =
    pd.DataFrame()):
        super(ManualSourceMapper, self).__init__(new_report)
        self._actual = 1
        self.title = 'map by manual rules'
        self.filters = self.get_filters(filters)
        self.map_by_filters()
        self.add_actual()
        self.update_df()

    def get_filters(self, filters):
        if len(filters.index) == 0:
            filters = self.get_filters_from_db()
        filters = self.get_filters_names(filters)
        return filters

    def get_filters_from_db(self):
        q = 'SELECT * FROM so_filter_rules'
        filters = pd.read_sql(q, current_app.config['ENGINE'])
        return filters

    def get_filters_names(self, filters):
        q = 'SELECT id,name FROM so_filter'
        filters_names = pd.read_sql(q, current_app.config['ENGINE'])
        filters = filters.merge(filters_names, how='left',
                                left_on='filter_id', right_on='id')
        return filters

    def map_by_filters(self):
        self.filters = self.filters.sort_values(by=['filter_id',
                                                    'filter_column'])
        for id in self.filters['filter_id'].unique().tolist():
            mask = self.filters['filter_id'] == id
            filter_df = self.filters.loc[mask, :].reset_index(drop=True)
            self.set_rows_by_filter(filter_df)
        self.get_mapped_rows()

    def set_rows_by_filter(self, filter_df: pd.DataFrame):
        for i, rule in filter_df.iterrows():
            if i == 0:
                title = rule['name']
                code_role = rule['so_code_role']
                word_rules = dict()
            cur_column = rule['filter_column']
            if cur_column not in word_rules:
                word_rules[cur_column] = set()
            if rule['value_type'] == 'FULL':
                mask = self.unmapped_df[cur_column] == rule['value']
            else:
                mask = self.get_filter_by_regex_rows(self.unmapped_df,
                                                     [cur_column],
                                                     rule['value'])
            filter_rows = set(self.unmapped_df.index[mask].unique())
            word_rules[cur_column].update(filter_rows)

        for i, word in enumerate(word_rules):
            if i == 0:
                available_indexes = word_rules[word]
            else:
                available_indexes.intersection_update(word_rules[word])
        rule_df = self.unmapped_df.ix[available_indexes]
        self.title = title
        self.add_info_tag(rule_df)
        rule_df['so_code_role'] = code_role
        self.unmapped_df.update(rule_df)
