from app.main.util.dataframes.dataframe import DF


def get_view_json(df):
    cols = ['so_report_lvl', 'so_block_report', 'so_date', 'so_fact_hc',
            'so_source_hc', 'so_vacancy_hc']
    block_df = df[cols].groupby(
        ['so_report_lvl', 'so_block_report']).sum().reset_index()
    block_df.replace({'so_report_lvl': {'ЦА': 'ca', 'ТБ': 'tb',
                                        'ПЦП': 'pcp'}}, inplace=True)
    block_df['diff'] = block_df['so_source_hc'] - block_df['so_fact_hc']
    block_df = block_df.sort_values(by=['so_block_report']).reset_index(
        drop=True)
    json = dict(headers=[], rows=[])
    block_d = {}
    for i, row in block_df.iterrows():
        if row['so_block_report'] != block_d.get('block', ''):
            if i != 0:
                json['rows'].append(block_d)
                block_d = {}
            block_d['block'] = row['so_block_report']
            block_d['directions'] = list()
        direction = dict()
        values = dict()
        direction['name'] = row['so_report_lvl']
        values['so_source_hc'] = round(row['so_source_hc'], 2)
        values['so_fact_hc'] = round(row['so_fact_hc'], 2)
        values['so_vacancy_hc'] = round(row['so_vacancy_hc'], 2)
        values['diff'] = round(row['diff'], 2)
        direction['values'] = values
        block_d['directions'].append(direction)
    json['headers'] = [
        {
            'id': 'so_fact_hc',
            'title': "Факт на 29.02.2020"
        },
        {
            'id': 'so_source_hc',
            'title': "Штат на 29.02.2020"
        },
        {
            'id': 'so_vacancy_hc',
            'title': "Вакансии на 29.02.2020"
        },
        {
            'id': 'diff',
            'title': "Дельта"
        },
    ]
    return json


def get_view_json_type(entity, type):
    headers = get_headers_by_type(entity, type)
    rows = get_rows_by_type(entity=entity, headers=headers, type=type)


def get_headers_by_type(entity, type):
    headers = list()
    for row in type['rows']:
        headers.append(row)
    dates = DF(entity).get_unique_by_column('so_date')
    for date in dates:
        for value in type['values']:
            value_obj = dict(id=value['id'],
                             value=f"{value['value']} на {date}",
                             nested=[])
            value_obj = get_columns_header(entity, value_obj, type['columns'])
            headers.append(value_obj)
    return headers


def get_columns_header(entity, value_obj, type_columns):
    for i, column in enumerate(type_columns):
        col_values = DF(entity).get_unique_by_column(column['id'])
        for val in col_values:
            nested_obj = dict(id=f"{value_obj['id']}_{val.lower()}",
                              value=val)
            if len(type_columns) != i + 1:
                entity_filter = entity[column['id']] == val
                nested_obj['nested'] = []
                nested_obj['nested'].extend(get_columns_header(
                    entity=entity[entity_filter],
                    value_obj=nested_obj,
                    type_columns=type_columns[i + 1:]))
            value_obj['nested'].append(nested_obj)

    return value_obj


def get_rows_by_type(entity, headers, type):
    entity.set_index(entity['id'], inplace=True)
    value_cols = list(type['values'].keys())

    for i, row in entity.iterrows():
        row_d = dict(id=i, rowValues=headers)
        for val in row_d['rowValues']:
            grouped_cols = row[val['id']].extend(value_cols)
            grouped_df = entity.groupby(grouped_cols)


a = [
    {'id': 'so_block_report', 'value': 'Блок'},
    {'id': 'so_fact_hc', 'value': 'Факт на 2020-02-29',
     'nested': [
         {'id': 'so_fact_hc_территориальные банки',
          'value': 'Территориальные банки'},
         {'id': 'so_fact_hc_подразделения центрального подчинения',
          'value': 'Подразделения центрального подчинения'},
         {'id': 'so_fact_hc_пао «сбербанк россии» (ца)',
          'value': 'ПАО «Сбербанк России» (ЦА)'},
         {'id': 'so_fact_hc_от-ления сбербанка россии г.москвы',
          'value': 'От-ления Сбербанка России г.Москвы'}]},
    {'id': 'so_source_hc', 'value': 'Штат на 2020-02-29',
     'nested': [
         {'id': 'so_source_hc_территориальные банки',
          'value': 'Территориальные банки'},
         {'id': 'so_source_hc_подразделения центрального подчинения',
          'value': 'Подразделения центрального подчинения'},
         {'id': 'so_source_hc_пао «сбербанк россии» (ца)',
          'value': 'ПАО «Сбербанк России» (ЦА)'},
         {'id': 'so_source_hc_от-ления сбербанка россии г.москвы',
          'value': 'От-ления Сбербанка России г.Москвы'}]}
]

view_types = [
    {'type_id': 1,
     'entities': [{'id': 15}],
     'rows': [
         {'id': 'so_block_report', 'value': 'Блок'},
     ],
     'values': [
         {'id': 'so_fact_hc', 'value': 'Факт', },
         {'id': 'so_source_hc', 'value': 'Штат'},
     ],
     'columns': [
         {'id': 'so_lvl_2_name', 'value': 'Уровень 2'},
     ]}
]
