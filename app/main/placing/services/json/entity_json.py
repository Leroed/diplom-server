import pandas as pd
from ..mapper import COLUMNS_PAO, COLUMNS_DZO, COLUMNS_ROLES
from typing import List


def get_entity_details_json(entity: pd.DataFrame):
    entity_j = dict(unpointed=[], actual=[], unmapped=[])
    all_fact = 0
    entity.loc[entity['INFO'].isna(), 'INFO'] = 'Unmapped'
    for tag in entity['INFO'].unique().tolist():
        tag_df = entity.loc[entity['INFO'] == tag, :]
        tag_info = dict()
        rows = dict(duplicates=0, success=tag_df.shape[0])
        tag_info['rows'] = rows
        if tag == 'Unmapped':
            tag_info['headcount'] = tag_df['so_fact_hc'].sum()
            entity_j['unmapped'].append(tag_info)
        else:
            tag_info['name'] = tag
            actual = tag_df['so_actual'].unique().tolist()[0]
            if int(actual) != 1:
                tag_info['headcount'] = tag_df['so_fact_hc'].sum()
                entity_j['unpointed'].append(tag_info)
            else:
                all_fact += tag_df['so_fact_hc'].sum()
                entity_j['actual'].append(tag_info)
    # entity_j['actual']['headcount'] = all_fact
    return entity_j


def get_entity_json(entity: pd.DataFrame, header_name='headers'):
    entity_j = dict(maxLvlNested=1, rows=[])
    entity_j[header_name] = get_headers(entity.columns, COLUMNS_PAO)
    entity.set_index(entity['id'], inplace=True)
    entity_j['rows'] = get_rows(entity, entity_j[header_name])
    return entity_j


def get_headers(columns: pd.DataFrame.columns, directory_columns):
    headers = list()
    for i, col in enumerate(columns):
        column_d = dict(id=col,
                        value=directory_columns.get(col, [col])[0],
                        sort=None,
                        isChecked=True if i < 6 else False)
        headers.append(column_d)
    return headers


def get_rows(entity: pd.DataFrame, headers: List = None):
    rows = list()
    view_rows = get_columns_by_headers(entity, headers)
    for i, row in view_rows.iterrows():
        row_d = dict(id=i, values=row.to_dict())
        rows.append(row_d)
    return rows


def get_roles_rows(roles: pd.DataFrame, entity: pd.DataFrame,
                   headers: List = None):
    rows = list()
    hc_columns = ['so_source_hc', 'so_fact_hc', 'so_vacancy_hc']
    hc_df = entity[hc_columns + ['so_code_role']].groupby(by=[
        'so_code_role']).sum().reset_index()
    roles.set_index(roles['Code'], inplace=True)
    view_rows = get_columns_by_headers(roles, headers)
    for i, row in view_rows.iterrows():
        row_d = dict(id=i, values=row.to_dict())
        hc_values = list()
        hc_attrs = hc_df[hc_df['so_code_role'] == i]
        if len(hc_attrs.index) == 0:
            row_d['headcount'] = []
        else:
            hc_attrs = hc_attrs[hc_columns].to_dict('records')[0]
            for attr in hc_attrs:
                attr_dict = dict(id=attr,
                                 title=COLUMNS_PAO.get(attr, [attr])[0],
                                 value=round(hc_attrs[attr], 2))
                hc_values.append(attr_dict)
            row_d['headcount'] = hc_values
        rows.append(row_d)
    return rows


def get_columns_by_headers(df: pd.DataFrame, headers: List = None):
    if headers:
        headers_list = list()
        for h in headers:
            headers_list.append(h['id'])
        return df[headers_list].fillna('')
    else:
        return df.fillna('')


def get_unmapped_json(entity):
    return get_entity_json(entity)


def get_mapped_json(entity: pd.DataFrame, roles: pd.DataFrame):
    entity_j = dict(maxLvlNested=1, rows=[])
    entity_j['roleHeaders'] = get_headers(roles.columns, COLUMNS_ROLES)
    entity_j['mappingRowsHeaders'] = get_headers(entity.columns, COLUMNS_PAO)
    entity_j['rows'] = get_roles_rows(roles=roles, entity=entity,
                                      headers=entity_j['roleHeaders'])
    return entity_j
