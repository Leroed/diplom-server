from app.main import celery

import pandas as pd
from datetime import datetime
from flask import current_app
import json


def create_report(name):
    name = name if name else \
        'Отчет на ' + datetime.now().date().strftime('%Y-%m-%d')
    login = 'admin'
    q = f"INSERT INTO so_report(name,login) " \
        f"VALUES (N'{name}','{login}')"
    current_app.config['ENGINE'].connect().execute(q)
    response_object = {
        'status': 'success',
        'message': 'Successfully created.',
    }
    return response_object


def get_all_reports():
    blocks = pd.read_sql('''
    SELECT id, name, headcounts FROM blocks
    ''',current_app.config['ENGINE'])
    print('blocks', blocks)
    reports = pd.read_sql('SELECT * FROM so_report',
                          current_app.config['ENGINE'])
    reports = reports.sort_values(by=['created_date'],ascending=False)
    reports_json = []
    if len(reports.index) > 0:
        reports_json = json.loads(reports.to_json(orient='records'))
        spr_entities = pd.read_sql('SELECT id as spr_id,name FROM so_entity',
                                   current_app.config['ENGINE'])

        entities = pd.read_sql('SELECT id,status,report_id,entity_id '
                               'FROM so_report_entity',
                               current_app.config['ENGINE'])
        if len(entities.index) > 0:
            entities = entities.merge(spr_entities, how='left',
                                      left_on=['entity_id'],
                                      right_on=['spr_id'])
        for report in reports_json:
            filter = (entities.report_id == report['id'])
            if filter.size > 0:
                report['loads'] = json.loads(entities[filter][['id', 'name',
                                                               'status',
                                                               'entity_id'
                                                               ]].to_json(
                    orient='records'))
            else:
                report['loads'] = []

    return reports_json


def delete_report(id):
    current_app.config['ENGINE'].connect().execute(f'DELETE FROM so_report '
                                                   f'WHERE id={id}')
