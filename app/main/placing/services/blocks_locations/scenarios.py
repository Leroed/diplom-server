from ..blocks_locations import get_blocks, get_locations

def get_scenarios():
    blocks = get_blocks()
    locations = get_locations()
    scenarios = []
    for i in range(4):

        newScenario = {
            'id': i,
            'name': f'Сценарий-{i}',
            'blocks': list(map(lambda block : (
                {
                    'id': block['id'],
                    'name': block['name'],
                    'isModelling': True if block['headcounts'] > 1000 else False,
                    'location': 0 if block['headcounts'] > 1000 else locations[0]['id']
                 }
            ), blocks)),
            'capacity': list(map(lambda location: ({
                'id': location['id'],
                'name': location['name'],
                'capacity': location['capacity'],
                'capacity_limit_value': 0.85,
            }),locations))
        }
        scenarios.append(newScenario)
    return scenarios
