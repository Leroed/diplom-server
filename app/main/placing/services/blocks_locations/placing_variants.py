import random

from ..blocks_locations import get_blocks, get_locations

def get_placing_variants_locations():
    blocks = get_blocks()
    locations = get_locations()
    placing_variants = []

    for i in range(30):
        qualityVal = random.random() * 15;
        new_placing_row = {
            "id": i,
            "blocks": list(map(lambda block : (
                {
                    'id': block['id'],
                    'name': block['name'],
                    'location': locations[int(random.random()*2)]['id']
                 }
            ), blocks)),
            "locations": list(map(lambda location : (
                {
                    'id': location['id'],
                    'capacity_procent': 75 + (random.random() * 15)
                 }
            ), locations)),
            "quality": qualityVal,
            "stability": (qualityVal/15)
        }
        placing_variants.append(new_placing_row)
    placing_variants.sort(key=lambda x: x["quality"], reverse=True)
    return placing_variants
