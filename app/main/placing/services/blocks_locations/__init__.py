import pandas as pd
from flask import current_app
import json


def get_locations():
    locations = pd.read_sql('''
            SELECT id,name,capacity FROM locations
            ''', current_app.config['ENGINE']);
    dictColors = {1: "rgb(28, 122, 39)", 2: "rgb(230,230,230)"}
    return (
        list(map(lambda x: {
            "id": x['id'],
            "name": x['name'],
            "capacity": x['capacity'],
            "color": dictColors[x['id']] if dictColors[x['id']] else "red"
        },json.loads(locations.to_json(orient='records'))))
    )

def get_blocks():
    blocks = pd.read_sql('''
        SELECT id, name, headcounts FROM blocks
        ''', current_app.config['ENGINE'])
    print('blocks',json.loads(blocks.to_json(orient='records')), type(json.loads(blocks.to_json(orient='records'))))
    return json.loads(blocks.to_json(orient='records'))
