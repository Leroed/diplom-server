from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from celery import Celery
from .config import config_by_name,Config

db = SQLAlchemy()
flask_bcrypt = Bcrypt()

# Initialize Celery
celery = Celery(__name__, broker=Config.CELERY_BROKER_URL)


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_by_name[config_name])
    app.config.from_pyfile('config.py')
    CORS(app)
    db.init_app(app)
    flask_bcrypt.init_app(app)

    celery.conf.update(app.config)

    return app
