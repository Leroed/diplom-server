from io import StringIO, BytesIO
import xlsxwriter


def export_to_excel(df):
    towrite = BytesIO()
    df.to_excel(towrite, engine='xlsxwriter')  # write to BytesIO buffer
    towrite.seek(0)
    return towrite.getvalue()
