import werkzeug
from flask_restplus import reqparse
from flask import abort
import json


class InputParser():
    def __init__(self):
        self._parser = reqparse.RequestParser()
        self._mimetype = []

    def get_parser(self):
        return self._parser

    def set_parser(self, *args, **kwargs):
        self._parser.add_argument(*args, **kwargs)

    def get_args(self):
        return self._parser.parse_args()


class JsonParser(InputParser):
    def __init__(self):
        super(JsonParser, self).__init__()

        self._parser.add_argument('json',
                                  type=json.loads,
                                  default = [],
                                  required=True,
                                  help='JSON content')


class ExcelParser(InputParser):
    def __init__(self):
        super(ExcelParser, self).__init__()

        self._parser.add_argument('excel_file',
                                  type=werkzeug.datastructures.FileStorage,
                                  location='files',
                                  required=True,
                                  help='XLSX file')

        self._mimetype = [
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']

    def get_args(self):
        args = self._parser.parse_args()
        file = args['excel_file']
        if self._mimetype:
            if file.mimetype in self._mimetype:
                return args
            abort(500, 'Invalid format file')
        return args
