class Config:
    DEBUG = False
    TESTING = False
    ASSETS_DEBUG = False
    SQLALCHEMY_DATABASE_URI = False
    ENGINE = False
    # Celery configuration
    CELERY_BROKER_URL = 'redis://localhost:6379/0'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
    DATA_FOLDER = '_local'
    ENTITIES_FOLDER ='_entities'
    SPR_FOLDER ='_sprs'


class DevelopmentConfig(Config):
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ASSETS_DEBUG = True


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc://supermegaadmin:123QWEasd@sql541'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ASSETS_DEBUG = True


config_by_name = dict(
    development=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)
