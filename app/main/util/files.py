import os
from flask import current_app
from werkzeug.datastructures import FileStorage
from datetime import datetime
import csv
import xlrd


def save_file(file: FileStorage):
    destination = os.path.join(os.environ['PWD'],
                               current_app.config.get('DATA_FOLDER'))
    if not os.path.exists(destination):
        os.makedirs(destination)
    filename = file.filename
    extension = get_file_extension(filename)
    dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    hashfile = hash(file.filename + dt)
    local_filename = f'{destination}/{hashfile}{extension}'
    file.save(local_filename)
    return local_filename


def get_entity_file(id):
    if id:
        file_path = os.path.join(current_app.config['ENTITIES_FOLDER'],
                                 f'{id}.csv')
        if os.path.isfile(file_path):
            return file_path
    else:
        file_path = os.path.join(current_app.config['ENTITIES_FOLDER'],
                                 '3.csv')
        return file_path


def get_directory_file(name: str) -> str:
    """
    Get directory from _sprs
    :param name: Name of directory
    :return: filepath of directory
    """
    file_path = os.path.join(current_app.config['SPR_FOLDER'],
                             f'{name}.csv')
    if os.path.isfile(file_path):
        return file_path


def parse_xlsx_to_csv(xlsx, sheet_index=0):
    xlsx_name = os.path.splitext(xlsx)[0]
    wb = xlrd.open_workbook(xlsx)
    sh = wb.sheet_by_index(sheet_index)
    filename = f'{xlsx_name}.csv'
    csv_file = open(filename, 'w')
    wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))
    csv_file.close()

    return filename


def get_file_extension(filename):
    return os.path.splitext(filename)[1]
