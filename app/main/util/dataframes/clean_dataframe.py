import pandas as pd
from typing import List, Union, Dict
from .dataframe import DF


class CleanDF(DF):
    def __init__(self, df, columns=None):
        super(CleanDF, self).__init__(df)
        self.del_empty_columns()
        self.del_empty_rows()

    def del_empty_rows(self,
                       check_columns: List[str] = None) -> None:
        """
        Delete rows which all values is NULL
        :param check_columns: column which will be checked by NAN
        Example. {'id':int,'date':datetime}
        :return None
        """
        if check_columns:
            self.df = self.df.dropna(subset=check_columns)
        self.df = self.df[~self.df.isnull().all(1)]

    def del_empty_columns(self) -> None:
        """
        Get slice of df without unnamed columns
        :return None
        """
        self.df = self.df.loc[:, ~self.df.columns.str.contains('^Unnamed')]

    def set_column_names(self, base_columns: List[str]) -> \
            pd.DataFrame.columns:
        """
        Rename columns to template column names.
        :param base_columns: Template of base columns
        :return Updated columns of input DataFrame
        """
        real_cols = self.df.columns.tolist()
        for index, real_col in enumerate(real_cols):
            for base_col in base_columns:
                if real_col in base_columns[base_col]:
                    real_cols[index] = base_col
                    break
        return real_cols

    @staticmethod
    def check_unnamed_column(column: str) -> bool:
        """
        Check if column name is 'Unnamed%'
        :param column: column name
        :return: True if it's unnamed column
        """
        return 'Unnamed' in column



