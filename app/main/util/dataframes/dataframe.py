import pandas as pd
from numpy import nan
from typing import Dict, List


class DF:
    def __init__(self, df: pd.DataFrame):
        self.df = df

    def set_column_types(self, col_types: Dict) -> None:
        """
        Replace columns types by template types.
        :param col_types: Columns with template type

        """
        for col in self.df.columns:
            if col in col_types:
                if col_types[col] == 'float' and self.df[col].dtype == \
                        'object':
                    self.df[col] = self.df[col].str.replace(",", ".")
                self.df[col] = self.df[col].astype(col_types[col])

    @staticmethod
    def add_new_columns(df: pd.DataFrame, *args: str) -> None:
        """

        :param df:
        :param args:
        """
        columns = df.columns
        for arg in args:
            if arg not in columns:
                df.loc[:, arg] = nan

    def get_unique_by_column(self, column: str) -> List:
        if column in self.df:
            return self.df[column].unique().tolist()
        return []
