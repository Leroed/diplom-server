from . import api
from flask_restplus import Resource
from app.main.placing.services.mapper.mapper import source_mapper
from flask import jsonify
local_route = '/celery'



@api.route(f'{local_route}/source_mapper/<string:task_id>')
@api.param('task_id', 'Celery task identifier')
class CeleryTask(Resource):
    @api.doc('source_mapper_status')
    def get(self,task_id):
        '''Status of source mapping  by task id'''
        task = source_mapper.AsyncResult(task_id)
        if task.state !='FAILURE':
            response = {
                'state': task.state,
                'all': task.info.get('all',0),
                'done': task.info.get('done',0),
                'description': task.info.get('title',''),
            }
        else:
            response = {'state': task.state}
        print(response)
        return {'task':response}