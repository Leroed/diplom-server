from flask_restplus import Namespace

api = Namespace('basic', description='Global functional', path='/')

from . import celery_tasks