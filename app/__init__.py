from flask_restplus import Api
from flask import Blueprint

from app.main.placing.routes import apiTest as source_api, blocks_locations_api, departments_buildings_api
from app.main.basic.routes import api as basic_api

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='PLACING REST-api',
          version='1.0',
          description=''
          )

api.add_namespace(basic_api)
api.add_namespace(source_api)
api.add_namespace(blocks_locations_api)
api.add_namespace(departments_buildings_api)
