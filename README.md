# Diplom-server

Flask server for placing on locations and buildings

# Installing Libs  

## Creating Virtual Environment  

MAC OS

### `python3 -m venv env`

Windows

### `python -m venv env`

## Activate Virtual Environment  

### `. env/bin/activate`

## Downloding Libs

### `pip install -r requirements.txt`

# Start Project 

MAC OS

### `python3 manage.py run`

Windows

### `python manage.py run`